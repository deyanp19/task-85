import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const ul = document.querySelector("ul");
  function checkStatusofRes(res) {
    if (res.status>=200&&res.status<300) {
      return Promise.resolve(res);
    } else {
      return Promise.reject(new Error(res.statusText))
    }
  }
function toJson(data) {
  return data.json();
}

  fetch('https://pokeapi.co/api/v2/pokemon?limit=10')
  .then(checkStatusofRes).then(toJson).then((x)=>{
    
    console.log(x.results);
    x.results.map(data=>{
      let liEl=document.createElement('li');
      liEl.innerText=data.name;
      ul.appendChild(liEl)
    })
  })
});
